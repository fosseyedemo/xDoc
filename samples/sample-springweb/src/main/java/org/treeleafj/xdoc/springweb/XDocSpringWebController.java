package org.treeleafj.xdoc.springweb;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.treeleafj.xdoc.XDoc;
import org.treeleafj.xdoc.format.http.HtmlForamt;
import org.treeleafj.xdoc.spring.framework.SpringWebHttpFramework;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * XDoc的Spring Web入口
 *
 * @author leaf
 * @date 2021-02-09 16:49
 */
@RequestMapping("xdoc")
public class XDocSpringWebController {

    private Logger logger = LoggerFactory.getLogger(XDocSpringWebController.class);

    @Value("${xdoc.enable:true}")
    private boolean enable;

    @Value("${xdoc.sourcePath:}")
    private String sourcePath;

    @Value("${xdoc.version:1.0}")
    private String version;

    @Value("${xdoc.title:SpringWeb接口文档}")
    private String title;

    private String html;

    @PostConstruct
    public void _init() {
        //使用多线程异步去初始化,尽量不阻塞系统启动
        try {
            Thread thread = new Thread(this::init);
            thread.start();
        } catch (Exception e) {
            logger.error("start up XDoc error", e);
        }
    }

    public void init() {
        if (!this.enable) {
            return;
        }

        String path = this.sourcePath;

        if (StringUtils.isBlank(path)) {
            path = ".";//默认为当前目录
        }

        List<String> paths = Arrays.asList(path.split(","));

        List<File> srcDirs = new ArrayList<>(paths.size());
        List<String> srcDirPaths = new ArrayList<>(paths.size());

        try {
            for (String s : paths) {
                File dir = new File(s);
                srcDirs.add(dir);
                srcDirPaths.add(dir.getCanonicalPath());
            }
        } catch (Exception e) {
            logger.error("获取源码目录路径错误", e);
            return;
        }

        logger.debug("starting XDoc, source path:{}", srcDirPaths);

        XDoc xDoc = new XDoc(srcDirs, new SpringWebHttpFramework());

        try {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("version", this.version);
            properties.put("title", this.title);

            //生成接口文档的html
            try(ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                xDoc.build(out, new HtmlForamt(), properties);
                html = out.toString("UTF-8");
            } catch (Exception e) {
                logger.error("生成html文档失败");
            }

            logger.info("start up XDoc");
        } catch (Exception e) {
            logger.error("start up XDoc error", e);
        }

    }

    /**
     * 跳转到xdoc接口文档首页
     */
    @GetMapping({"index", "index.html"})
    public void index(HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Type", "text/html;charset=utf-8");
        response.getWriter().println(html);
    }

    /**
     * 重新构建文档
     *
     * @return 文档页面
     */
    @GetMapping({"rebuild", "rebuild.html"})
    public String rebuild() {
        init();
        return "redirect:index.html";
    }
}
